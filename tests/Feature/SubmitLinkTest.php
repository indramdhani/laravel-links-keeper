<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;

class SubmitLinkTest extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    function guest_can_submit_a_new_link(){
        $response = $this->post('/submit',[
            'title' => 'Example Title',
            'url'   => 'http://example.com',
            'description'   => 'Example description',
        ]);

        $this->assertDatabaseHas('links',[
            'title' => 'Example Title'
        ]);

        $response
            ->assertStatus(302)
            ->assertHeader("Location",url('/'));

        $this->get('/')->assertSee("Example Title");
    }

    function link_is_not_created_if_validation_fails(){
        $response = $this->post('/submit');
        $response->assertSessionHasErrors(['title', 'url', 'description']);
    }

    function link_is_not_created_with_an_invalid_url(){
        $this->withoutExceptionHandling();
        $cases = ['//invalid-url.com','/invalid-url','foo.com'];

        foreach ($cases as $key => $case) {
            try{
                $response = $this->post('/submit', [
                    'title' => "Example title",
                    'url'   => $case,
                    'description'   => 'Example Description'
                ]);
            } catch(ValidationException $e) {
                $this->assertEquals(
                    "the url format is invalid.",
                    $e->validator->errors()->first("url")
                );
                continue;
            }
            $this->fail('The URl $case passed validation when it should have failed');
        }
    }

    function max_length_fails_when_too_long(){
        $this->withoutExceptionHandling();

        $title = str_repeat('a', 256);
        $description = str_repeat('a', 513);
        $url = 'http://';
        $url .= str_repeat('a',256 - strlen($url));

        try {
            $this->post('/submit', compact('title','url','description'));
        } catch(ValidationException $e){
            $this->assertEquals(
                'The title may not be greater than 255 characters.',
                $e->validator->errors()->first('title')
            );

            $this->assertEquals(
                'The URL may not be greater than 255 characters.',
                $e->validator->errors()->first('url')
            );

            $this->assertEquals(
                'The description may not be greater than 512 characters.',
                $e->validator->errors()->first('description')
            );
            return;
        }
        $this->fail('Max lenght should be trigger a validationexception');
    }
  
    function max_length_succeeds_when_under_max(){
        $url = 'http://';
        $url .= str_repeat('a',255-strlen($url));
        
        $data = [
            'title' => str_repeat('a', 255),
            'url'   => $url,
            'description'   => str_repeat('a', 512)
        ];
        $this->post('/submit', $data);
        $this->assertDatabaseHas('links',$data);
    }
}
