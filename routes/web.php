<?php
// use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// indramdhani:index link
Route::get('/', 'LandingController@index');
Route::get('/links', 'LinksController@index')->name("links")->middleware('auth');
// indramdhani:go to form to add
Route::get('/links/create', 'LinksController@create')->name('links/create')->middleware('auth');
// indramdhani:store the data from add
Route::post('/links', 'LinksController@store')->name('links/store')->middleware('auth');
// indramdhani:update the data from edit
Route::put('/links/{id}', 'LinksController@update')->name('links/update')->middleware('auth');
// indramdhani:delete the link item from index
Route::delete("/links/{id}", 'LinksController@destroy')->middleware('auth');
// indramdhani:get data from index
Route::get("/links/{id}", 'LinksController@edit')->middleware('auth');
Route::get("/api/search", 'LinksController@search')->name('api/search')->middleware('auth');

Route::get('/categories', 'CategoriesController@index')->name("categories")->middleware('auth');
Route::get('/categories/create', 'CategoriesController@create')->name('categories/create')->middleware('auth');
Route::post('/categories', 'CategoriesController@store')->name('categories/store')->middleware('auth');
Route::get("/categories/{id}", 'CategoriesController@edit')->middleware('auth');
Route::put('/categories/{id}', 'CategoriesController@update')->name('categories/update')->middleware('auth');
Route::delete("/categories/{id}", 'CategoriesController@destroy')->middleware('auth');

Route::get('/tags', 'TagController@index')->name("tags")->middleware('auth');
Route::get('/tags/create', 'TagController@create')->name('tags/create')->middleware('auth');
Route::post('/tags', 'TagController@store')->name('tags/store')->middleware('auth');
Route::get("/tags/{id}", 'TagController@edit')->middleware('auth');
Route::put('/tags/{id}', 'TagController@update')->name('tags/update')->middleware('auth');
Route::delete("/tags/{id}", 'TagController@destroy')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/profile', 'HomeController@profile')->name('profile')->middleware('auth');
Route::put('/profile/update/{id}', 'HomeController@profileUpdate')->name('profile/update')->middleware('auth');
Route::put('/profile/password/{id}', 'HomeController@passwordUpdate')->name('profile/password')->middleware('auth');
