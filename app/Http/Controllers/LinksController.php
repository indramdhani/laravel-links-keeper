<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Alert;

class LinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $links = \App\Link::where('user_id', Auth::id())->get();
        return view('link/index', ['links'=> $links]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = \App\Categories::where('user_id', Auth::id())->get();
        $tags = \App\Tag::where('user_id', Auth::id())->get();
        return view('link/submit', ['categories'=> $categories, 'tags' => $tags]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|max:255',
            'url' => 'required|url|max:255',
            'category_id'   => 'required',
            'description' => 'required|max:512',
        ]);
        $data["user_id"] = Auth::id();
       
        $link = tap(new \App\Link($data))->save();
        $link_tags = \App\Tag::find($request->tag_id);
        $link->tags()->attach($link_tags);
        toast('Data stored', 'success', 'top-right');
        return redirect('/links');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = \App\Categories::where('user_id', Auth::id())->get();
        $tags = \App\Tag::where('user_id', Auth::id())->get();
        $link = \App\Link::find($id);
        $selected_tags = array_pluck($link->tags, 'id');
        return view(
            'link/edit',
            [
            'link'=> $link,
            'categories'=> $categories,
            'tags' => $tags,
            'selected_tags' => $selected_tags
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'title' => 'required|max:255',
            'url' => 'required|url|max:255',
            'category_id' => 'required',
            'description' => 'required|max:512',
        ]);
        $link = tap(\App\Link::find($id))->update($data);
        $link_tags = \App\Tag::find($request->tag_id);
        $link->tags()->sync($link_tags);
        toast('Data updated', 'success', 'top-right');
        return redirect('/links');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $link = \App\Link::find($id);
        $link->tags()->detach();
        \App\Link::destroy($id);
        toast('Data removed', 'warning', 'top-right');
        return redirect('/links');
    }

    public function search(Request $request)
    {
        $error = ['error' => 'No results found, please try with different keywords.'];

        if ($request->has('q')) {
            $links = \App\Link::search($request->get('q'))->where('user_id', Auth::id())->get();
            // $links = \App\Link::search($request->get('q'))->get();

            return $links->count() ? $links : $error;
        }
        return $error;
    }
}
