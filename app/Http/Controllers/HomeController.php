<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Alert;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile/home');
    }

    public function profile()
    {
        $profile = Auth::user();
        return view('profile/profile', ['profile'=> $profile]);
    }

    public function profileUpdate(Request $request, $id)
    {
        if ($id == Auth::id()) {
            $data = $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255',
            ]);
            $user = tap(\App\User::find($id))->update($data);
            toast('Profile updated', 'success', 'top-right');
            return redirect('/profile');
        }
        // toast('Error Toast', 'error', 'top-right');
    }

    public function passwordUpdate(Request $request, $id)
    {
        if ($id == Auth::id()) {
            if (!(Hash::check($request->get("old_password"), Auth::user()->password))) {
                return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
            }

            if (strcmp($request->get('old_password'), $request->get('password')) == 0) {
                return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
            }
            $new_password['password'] = Hash::make($request->get("password"));
            $data = $request->validate([
                'old_password' => 'required|string|min:6',
                'password' => 'required|string|min:6|confirmed',
            ]);
            $user = tap(\App\User::find(Auth::id()))->update($new_password);
            toast('Password updated', 'success', 'top-right');
            return redirect('/profile');
        }
    }
}
