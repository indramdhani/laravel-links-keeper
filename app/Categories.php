<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categories extends Model
{
    use SoftDeletes;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

     protected $dates = ['deleted_at'];

     protected $fillable = [
         'title',
         'description',
         'user_id'
     ];

     public function user(){
        return $this->belongsTo(User::class);
     }

     public function links(){
         return $this->hasMany(Link::class, 'category_id', 'id');
     }

}
