<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "indramdhani",
            'email' => 'indramdhani@gmail.com',
            'password' => bcrypt('Hp09012010'),
        ]);
    }
}
