<?php

use Faker\Generator as Faker;

$factory->define(App\Tag::class, function (Faker $faker) {
    return [
        'title' => substr($faker->sentence(1), 0, -1),
        'user_id'   => 1
    ];
});
