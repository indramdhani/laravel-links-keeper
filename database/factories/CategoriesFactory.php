<?php

use Faker\Generator as Faker;

$factory->define(App\Categories::class, function (Faker $faker){
    return [
        'title' => substr($faker->sentence(2), 0, -1),
        'description'   => $faker->paragraph,
        'user_id'   => 1
    ];
});