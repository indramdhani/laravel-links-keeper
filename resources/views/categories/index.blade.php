@extends('layouts.app')
@section('content')
    <div class="container py-5">
        <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-secondary rounded shadow-sm">
            <div class="lh-100">
                <h1 class="mb-0 text-white lh-100">Categories</h1>
                <a class="text-white" href="{{ route('categories/create') }}">Add new category</a>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-center">
            <div class="card-columns">
                @foreach($categories as $category)
                <div class="card text-center">
                    <div class="card-body">
                        <h5 class="card-title">{{$category->title}}</h5>
                        <p class="card-text text-muted small">{{$category->description}}</p>
                        <p class="card-text">
                            <form class="d-inline" action="{{ url('/categories', ['id' => $category->id]) }}" method="post">
                                <input type="hidden" name="_method" value="delete" />
                                {!! csrf_field() !!}
                                <button class="btn btn-sm btn-outline-danger" type="submit">Delete</button>
                            </form>
                            <form class="d-inline" action="{{ url('/categories', ['id' => $category->id]) }}" method="post">
                                <input type="hidden" name="_method" value="get" />
                                {!! csrf_field() !!}
                                <button class="btn btn-sm btn-outline-info" type="submit">View</button>
                            </form>
                        </p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection