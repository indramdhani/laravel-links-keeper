@extends('layouts.app')
@section('content')
    <div class="container py-5">
        <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-secondary rounded shadow-sm">
            <div class="lh-100">
                <h1 class="mb-0 text-white lh-100">Tags</h1>
                <a href="{{route('tags')}}" class="text-white">Tags Index</a>
            </div>    
        </div>
        <form action="/tags" method="post">
            @if ($errors->any())
                <div class="alert alert-danger" role="alert">
                    Please fix the following errors
                </div>
            @endif

            {!! csrf_field() !!}
            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{ old('title') }}">
                @if($errors->has('title'))
                    <span class="help-block">{{ $errors->first('title') }}</span>
                @endif
            </div>
            <button type="submit" class="btn btn-outline-secondary">Submit</button>
        </form>
    </div>
@endsection