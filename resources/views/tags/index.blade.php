@extends('layouts.app')
@section('content')
    <div class="container py-5">
        <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-secondary rounded shadow-sm">
            <div class="lh-100">
                <h1 class="mb-0 text-white lh-100">Tags</h1>
                <a class="text-white" href="{{ route('tags/create') }}">Add new tag</a>
            </div>
        </div>
        <div class="d-flex align-items-center justify-content-center">
            <div class="card-columns">
                @foreach($tags as $tag)
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title text-center">{{$tag->title}}</h4>
                        <p class="card-text">
                            <form class="d-inline" action="{{ url('/tags', ['id' => $tag->id]) }}" method="post">
                                <input type="hidden" name="_method" value="delete" />
                                {!! csrf_field() !!}
                                <button class="btn btn-sm btn-outline-danger" type="submit">Delete</button>
                            </form>
                            <form class="d-inline" action="{{ url('/tags', ['id' => $tag->id]) }}" method="post">
                                <input type="hidden" name="_method" value="get" />
                                {!! csrf_field() !!}
                                <button class="btn btn-sm btn-outline-info" type="submit">View</button>
                            </form>
                        </p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

