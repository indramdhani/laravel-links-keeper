@extends('layouts.app')

@section('content')
<div class="container flex-center position-ref full-height">
    <div class="content">
        <div class="display-1 m-b-md">
            {{config('app.name')}}
        </div>
        <div class="h4 text-muted">
            a place to save your link
        </div>
    </div>
</div>
@endsection