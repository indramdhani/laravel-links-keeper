@extends('layouts.app')

@section('content')
<div class="container py-5">
    <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-secondary rounded shadow-sm">
        <div class="lh-100">
            <h1 class="mb-0 text-white lh-100">Dashboard</h1>
        </div>
    </div>
    <div class="row justify-content-center align-items-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class='button-group mb-3'>
                        <a href="{{ route('links') }}" class="btn btn-outline-secondary">{{ __('Links') }}</a>
                        <a href="{{ route('categories') }}" class="btn btn-outline-secondary">{{ __('Categories') }}</a>
                        <a href="{{ route('tags') }}" class="btn btn-outline-secondary">{{ __('Tags') }}</a>
                    </div>
                    @include('component.search')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
