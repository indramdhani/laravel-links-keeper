<div class="form-group">
    <div class="input-group input">
        <input type="text" class='form-control' id='search' placeholder="what are you looking for?" v-model='query'>
        <button type="button" class="btn btn-outline-secondary" @click="search()" v-if="!loading">search</button>
        <button type="button" class='btn btn-outline-success' disabled='disabled' v-if='loading'>searching...</button>
    </div>
</div>
<div class='alert alert-danger' role='alert' v-if='error'>
    <span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
    @{{error}}
</div>
<div id="links" class='d-flex align-items-center justify-content-center'>
    <div class="card-columns">
        <div class="card" v-for='link in links'>
            <div class="card-body">
                <h4 class="card-title text-center">@{{link.title}}</h4>
                <p class="card-text text-muted">@{{link.description}}</p>
                <p class='card-text'>
                    <a class="btn btn-sm btn-outline-danger" v-bind:href="'links/destroy/' + link.id">Delete</a>
                    <a class="btn btn-sm btn-outline-info" v-bind:href="'links/' + link.id">View</a>
                    <a class="btn btn-sm btn-outline-secondary" :href=link.url target="_blank">go to</a>
                </p>
            </div>
        </div>
    </div>
</div>